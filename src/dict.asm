%define POINTER_SIZE 8

global find_word
extern string_equals

find_word:
;rsi указатель на начало словаря
;rdi указатель на нуль-терминированную строку
    .loop:
        push rsi;
        add rsi, POINTER_SIZE
        call string_equals
        pop rsi

        cmp rax, 1
        je .found

        mov rsi, [rsi]; указатель на следующий элемент 
        test rsi, rsi
        jne .loop

    .error: 
        xor rax, rax
        ret

    .found:
        mov rax, rsi
        ret