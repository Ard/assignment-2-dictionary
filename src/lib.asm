%define TAB 0x9
%define NEW_LINE 0xA
%define SPACE 0x20

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
	    cmp byte [rax + rdi], 0 
	    je .end ; if ZF = 1 -> .end
        inc rax
    	jmp .loop
    .end: 
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    ;xor rax, rax

    call string_length ; в rax лежит кол-во символов
    mov rdx, rax; сохранить в rdx длину строки 
    mov rsi, rdi; сохраняет в rsi адрес начала строки
    mov rax, 1; номер системеного вызова write 
    mov rdi, 1; дескриптор stdout
    syscall
	
    ret

; Принимает код символа и выводит его в stdout
print_char:
    ;xor rax, rax
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1; кол-во байт для записи
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ;xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ;xor rax, rax
    mov rax, rdi
    mov r8, rsp; сохраняем адрес вершины аппаратного стека
    mov r9, 10; делитель
    
    dec rsp; в стек положили 0 - терминатор
    mov [rsp], byte 0  
    
    .loop:
	    xor rdx, rdx
        dec rsp
	    div r9
	    add rdx, '0'
	    mov [rsp], dl
	    test rax, rax
	    jnz .loop
    
    .end:
	    mov rdi, rsp
	    call print_string
	    mov rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ;xor rax, rax
    cmp rdi, 0
    jns .print

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi


    .print:
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    ;rdi И rsi содержат строки
    xor rcx, rcx; счетчик символа


    .loop:
        mov r8b, byte[rdi + rcx]
        mov r9b, byte[rsi + rcx]
        cmp r8b, r9b
        jne .end
        cmp r8b, 0
        je .good
        inc rcx
        jmp .loop

    .end:
        ret

    .good:
        inc rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax; номер системеного вызова read
    mov rdi, 0; дескриптор stdin
    push 0
    mov rdx, 1
    mov rsi, rsp;куда мы будем записывать значение
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ;rdi - адрес начала буфера, rsi - размер буфера
    xor rcx, rcx; счетчик символов
    mov r8, rdi
    mov r9, rsi

    .loop:
        push rcx
        call read_char
        pop rcx

        cmp al, 0x20;
        je .skip
        cmp al, 0x9
        je .skip
        cmp al, 0xA
        je .skip

        cmp rax, 0
        jz .end

        cmp r9, rcx; проверка не выходим ли за буфер
        je .exception

        mov [r8 + rcx], al
        inc rcx

        jmp .loop

    .skip: 
        test rcx, rcx
        jz .loop

    .end:
        xor rax, rax
        mov [r8+rcx], al
        mov rax, r8
        mov rdx, rcx
        ret

    .exception:
        mov rax, 0
        ret


;улучшенная версия read_word, которая читает слова с пробелами
read_line:
    ;rdi - адрес начала буфера, rsi - размер буфера
    xor rcx, rcx; счетчик символов
    mov r8, rdi
    mov r9, rsi

    .skip:
        push rcx
        call read_char
        pop rcx
        cmp al, SPACE
        je .skip
        cmp al, TAB
        je .skip
        cmp al, NEW_LINE
        je .skip

    .loop:
        cmp rax, 0
        jz .end

        cmp rax, NEW_LINE
        jz .end

        cmp r9, rcx; проверка не выходим ли за буфер
        je .exception

        mov [r8 + rcx], al
        inc rcx

        push rcx
        call read_char
        pop rcx

        jmp .loop

    .end:
        xor rax, rax
        mov [r8+rcx], al
        mov rax, r8
        mov rdx, rcx
        ret

    .exception:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax; число
    xor rdx, rdx; длина 
    mov r8, 10
    xor rcx, rcx

    .loop: 
        mov dl, byte[rdi+rcx]
        cmp rdx, 0x39
        jg .end
        cmp rdx, 0x30
        jl .end
        sub rdx, 0x30
        push rdx
        mul r8
        pop rdx
        add rax, rdx
        inc rcx
        jmp .loop

    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax; число
    xor rdx, rdx; длина(+знак)
    
    cmp byte [rdi], 0x2D
    je .negative
    call parse_uint
    ret

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret


; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx;

    .loop:
        cmp rdx, rcx
        je .endBuffer
        mov rax, [rdi + rcx]
        cmp rax, byte 0 
        je .end
        mov [rsi + rcx], rax
        inc rcx
        jmp .loop
        
    .endBuffer:
        mov rax, 0
        ret
    
    .end: 
        mov rax, rcx
        ret
