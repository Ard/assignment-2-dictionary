%define buffer_size 256
%define POINTER 8
%define STDERR 2
%define syscall_for_write 1

section .bss
buffer:
    resb buffer_size

section .data
buffer_overflow:
    db "Error: переполнение буфера", 0
key_not_found:
    db "Error: ключ не найден", 0
start_string: 
    db "Что вам нужно найти? ", 0
result_string:
    db "Значение вашего запроса: ", 0

section .text
	
%include "words.inc"

extern find_word
extern print_string
extern print_newline	
extern exit
extern string_length
extern read_line

global _start 
  

_start: 
    mov rdi, start_string ; начало работы
    call print_string

    mov rdi, buffer
    mov rsi, buffer_size
    call read_line
    test rax, rax
    je .bufferOverflow
    
    mov rdi, buffer
    mov rsi, NEXT

    call find_word
    test rax, rax
    je .notFound

    push rax
    mov rdi, result_string
    call print_string
    pop rax

    mov rdi, rax
    add rdi, POINTER
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax

	mov rdi, rax
	call print_string
    call print_newline
    mov rdi, 0
    call exit


    .bufferOverflow:
        mov rdi, buffer_overflow
        call .printErr
        mov rdi, 1
        call exit
    
    .notFound:
        mov rdi, key_not_found
        call .printErr
        mov rdi, 1
        call exit
        


    .printErr: ;Не забудьте, что сообщения об ошибках нужно выводить в stderr
        call string_length
        mov rsi, rdi
        mov rdi, STDERR
        mov rdx, rax    
        mov rax, syscall_for_write
        syscall
        ret